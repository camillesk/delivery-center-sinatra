## Delivery Center Ruby Back-end Test
Data: 29/04/20

O projeto tem como objetivo parsear um objeto JSON enviado via request para o endpoint `order/new`, a aplicação irá então fazer algumas validações e parsear o mesmo para que seja enviado para
 *https://delivery-center-recruitment-ap.herokuapp.com/* seguindo os critérios do teste.

Passo a passo para rodar o projeto:  (_É necessário ter o ruby e o mongoDB instalados na máquina_)

**No terminal**

> sudo systemctl start mongod

> bundle install

> bundle exec ruby app.rb

**Usando Postman ou Insomnia**

Para criar um novo pedido, que será enviado e salvo no banco de dados:

> Envie uma request do tipo POST para o endpoint `localhost:4567/order/new` com o JSON desejado.

*Caso esteja tudo correto, a API retornará um status 200 e mensagem OK. Caso haja algum erro, o mesmo será retornado.*

* Para verificar os pedidos existentes no banco de dados:

> Envie uma request do tipo GET para `localhost:4567/orders`
