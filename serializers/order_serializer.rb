# frozen_string_literal: true

# Order Serializer, used to return to our API which error prevented the object from saving
class OrderSerializer
  def initialize(order)
    @order = order
  end

  def as_json(*)
    data = {
      externalCode: @order.externalCode.to_s,
      storeId: @order.storeId.to_s,
      subTotal: @order.subTotal.to_s,
      deliveryFee: @order.deliveryFee.to_s,
      total_shipping: @order.total_shipping.to_s,
      total: @order.total.to_s,
      postalCode: @order.postalCode,
      country: @order.country,
      state: @order.state,
      city: @order.city,
      district: @order.district,
      street: @order.street,
      number: @order.number,
      complement: @order.complement,
      latitude: @order.latitude.to_s,
      longitude: @order.longitude.to_s,
      dtOrderCreate: @order.dtOrderCreate.to_s,
      customer: @order.customer,
      items: @order.items,
      payments: @order.payments
    }

    data[:errors] = @order.errors if @order.errors.any?
    data
  end
end
