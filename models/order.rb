# frozen_string_literal: true

# Order Model, including methods to create the json to send to the Delivery API, declare the fields of the class and some validations
class Order
  include Mongoid::Document

  field :externalCode, type: Integer
  field :storeId, type: Integer
  field :subTotal, type: Float
  field :deliveryFee, type: Float
  field :total_shipping, type: Float
  field :total, type: Float
  field :postalCode, type: String
  field :country, type: String
  field :state, type: String
  field :city, type: String
  field :district, type: String
  field :street, type: String
  field :number, type: String
  field :complement, type: String
  field :latitude, type: Float
  field :longitude, type: Float
  field :dtOrderCreate, type: String
  field :customer, type: Hash
  field :items, type: Array
  field :payments, type: Array

  validates :externalCode, presence: true
  validates :storeId, presence: true
  validates :subTotal, presence: true
  validates :deliveryFee, presence: true
  validates :total_shipping, presence: true
  validates :total, presence: true
  validates :postalCode, presence: true
  validates :country, presence: true
  validates :state, presence: true
  validates :city, presence: true
  validates :district, presence: true
  validates :street, presence: true
  validates :number, presence: true
  validates :complement, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :dtOrderCreate, presence: true
  validates :customer, presence: true
  validates :items, presence: true
  validates :payments, presence: true

  def parse_json(json_params)
    order = {
      "externalCode": json_params['id'],
      "storeId": json_params['store_id'],
      "subTotal": json_params['total_amount'],
      "deliveryFee": json_params['total_shipping'],
      "total_shipping": json_params['total_shipping'],
      "total": json_params['total_amount_with_shipping'],
      "postalCode": json_params['shipping']['receiver_address']['zip_code'],
      "country": json_params['shipping']['receiver_address']['country']['id'],
      "state": json_params['shipping']['receiver_address']['state']['name'],
      "city": json_params['shipping']['receiver_address']['city']['name'],
      "district": json_params['shipping']['receiver_address']['neighborhood']['name'],
      "street": json_params['shipping']['receiver_address']['street_name'],
      "number": json_params['shipping']['receiver_address']['street_number'],
      "complement": json_params['shipping']['receiver_address']['comment'],
      "latitude": json_params['shipping']['receiver_address']['latitude'],
      "longitude": json_params['shipping']['receiver_address']['longitude'],
      "dtOrderCreate": json_params['date_created'],
      "customer": {
        "externalCode": json_params['buyer']['id'],
        "name": json_params['buyer']['nickname'],
        "email": json_params['buyer']['email'],
        "contact": parse_contact(json_params)
      },
      "items": parse_items(json_params),
      "payments": parse_payments(json_params)
    }

    order
  end

  private

  def parse_items(json_params)
    items = []

    json_params['order_items'].map do |item|
      items << {
        "externalCode": item['item']['id'],
        "name": item['item']['title'],
        "price": item['unit_price'],
        "quantity": item['quantity'],
        "total": item['full_unit_price'],
        "subItems": item['sub_items'] || nil
      }
    end

    items
  end

  def parse_payments(json_params)
    payments = []

    json_params['payments'].map do |payment|
      payments << {
        "type": payment['payment_type'].upcase,
        "value": payment['total_paid_amount']
      }
    end

    payments
  end

  def parse_contact(json_params)
    "#{json_params['buyer']['phone']['area_code']}#{json_params['buyer']['phone']['number']}"
  end
end
