# frozen_string_literal: true

# Gems
require 'byebug'
require 'httparty'
require 'json'
require 'json-schema'
require 'mongoid'
require 'sinatra'

# Another files
require './models/order'
require './helpers/helper'
require './serializers/order_serializer'

# DB Setup
Mongoid.load! 'mongoid.config'

# Routes
get '/' do
  'Welcome to order parser for delivery center! :) -----
  To start using, send a JSON to localhost:4567/order/new ------
  You can see all orders saved on localhost:4567/orders'
end

post '/order/new' do
  payload = json_params
  validation = JSON::Validator.fully_validate(schema_validation, payload, errors_as_objects: true)

  unless validation.empty?
    errors = []
    validation.map { |e| errors << e[:message] }

    status 500
    body errors.to_json
    return
  end

  order = Order.new
  params = order.parse_json(payload)

  if order.update_attributes(params)
    headers = { 'X-Sent' => Time.now.strftime('%Hh%M - %d/%m/%y').to_s }

    response = HTTParty.post(
      'https://delivery-center-recruitment-ap.herokuapp.com/',
      body: params.to_json,
      headers: headers
    )

    status response.code
    body response.parsed_response
  else
    status 422
    body OrderSerializer.new(order).to_json
  end
end

get '/orders' do
  Order.all.to_json
end
