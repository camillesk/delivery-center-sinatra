# frozen_string_literal: true

helpers do
  def json_params
    JSON.parse(request.body.read)
  rescue StandardError
    halt 400, { message: 'Invalid JSON' }.to_json
  end

  def schema_validation
    schema = {
      'type' => 'object',
      'required' => %w[id store_id total_amount total_shipping total_amount_with_shipping date_created],
      'properties' => {
        'shipping' => {
          'type' => 'object',
          'properties' => {
            'receiver_address' => {
              'type' => 'object',
              'required' => %w[zip_code street_name street_number comment latitude longitude],
              'properties' => {
                'country' => {
                  'type' => 'object',
                  'required' => ['id']
                },
                'state' => {
                  'type' => 'object',
                  'required' => ['name']
                },
                'city' => {
                  'type' => 'object',
                  'required' => ['name']
                },
                'neighborhood' => {
                  'type' => 'object',
                  'required' => ['name']
                }
              }
            }
          }
        },
        'buyer' => {
          'type' => 'object',
          'required' => %w[id nickname email],
          'properties' => {
            'phone' => {
              'type' => 'object',
              'required' => %w[area_code number]
            }
          }
        },
        'order_items' => {
          'type' => 'array',
          'items' => {
            'required' => %w[unit_price quantity full_unit_price],
            'properties' => {
              'item' => {
                'type' => 'object',
                'required' => %w[id title]
              }
            }
          }
        },
        'payments' => {
          'type' => 'array',
          'items' => {
            'required' => %w[payment_type total_paid_amount]
          }
        }
      }
    }
    schema
  end
end
